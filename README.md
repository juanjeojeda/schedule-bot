# schedule-bot

Bots that run on a schedule

## Purpose

This repository contains the source code for all Python/Bash CKI bots that run
on a schedule, i.e. as a cron job.

## Available bots

### `git_cache_updater`

Update a cache of tar files containing git repositories in S3.

### `pipeline_bot`

Interact with a user in a merge request for the pipeline definition, and
trigger pipelines for it.

### `orphan_hunter`

Check whether the GitLab jobs for Pods spawned by `gitlab-runner` are still
running. Pods of already finished jobs are deleted.

#### Configuration

| Field            | Type   | Required | Description                                                                   |
|------------------|--------|----------|-------------------------------------------------------------------------------|
| `action`         | string | no       | `report` (default) or `delete` Pods                                           |
| `env_var_prefix` | string | no       | Kubernetes configuration environment variable prefix, defaults to `OPENSHIFT` |
| `gitlab_tokens`  | dict   | yes      | URL/environment variable pairs of GitLab instances and private tokens         |

#### Environment variables

| Name                | Secret | Required | Description                                                   |
|---------------------|--------|----------|---------------------------------------------------------------|
| `OPENSHIFT_KEY`     | yes    | yes      | OpenShift credentials                                         |
| `OPENSHIFT_SERVER`  | no     | yes      | OpenShift API server                                          |
| `OPENSHIFT_PROJECT` | no     | yes      | OpenShift namespace                                           |
| `GITLAB_TOKEN`      | yes    | yes      | GitLab private tokens as configured in `gitlab_tokens` above  |
| `IRCBOT_URL`        | no     | no       | IRC bot endpoint                                              |
| `SHORTENER_URL`     | no     | no       | URL shortener endpoint to link to GitLab jobs in IRC messages |

### `datawarehouse_report_crawler`

Submit all sent reports from a mailing list to the [Data Warehouse].

#### Configuration

| Field                 | Type   | Required | Description                                                   |
|-----------------------|--------|----------|---------------------------------------------------------------|
| `action`              | string | no       | `report` (default) or `submit` Reports                        |
| `mailing_list_url`    | string | yes      | URL of mailing list archive                                   |
| `datawarehouse_url`   | string | yes      | URL of Data Warehouse instance                                |
| `datawarehouse_token` | string | yes      | name of environment variable with Data Warehouse secret token |

#### Environment variables

| Name                  | Secret | Required | Description                                                              |
|-----------------------|--------|----------|--------------------------------------------------------------------------|
| `DATAWAREHOUSE_TOKEN` | yes    | yes      | Data Warehouse secret token as configured in `datawarehouse_token` above |

### `kernel_fixes`

Create a `kernel-fixes.json` file as required by the pipeline.

#### Configuration

| Field                | Type   | Required | Description                                                             |
|----------------------|--------|----------|-------------------------------------------------------------------------|
| `kernel.bucket_spec` | string | yes      | name of environment variable with the git cache bucket specification    |
| `kernel.filename`    | string | yes      | file name of the kernel archive in the git cache                        |
| `kernel.max_commits` | int    | no       | number of commits to check for fixes tags, defaults to 10000            |
| `output.bucket_spec` | string | yes      | name of environment variable with the kernel fixes bucket specification |
| `output.filename`    | string | no       | file name of the kernel fixes file, defaults to `kernel-fixes.json`     |

#### Environment variables

| Name                  | Secret | Required | Description                                                                |
|-----------------------|--------|----------|----------------------------------------------------------------------------|
| `BUCKET_GIT_CACHE`    | yes    | yes      | git-cache bucket specification as configured in `kernel.bucket_spec` above |
| `BUCKET_KERNEL_FIXES` | yes    | yes      | output bucket specification as configured in `output.bucket_spec` above    |

### `git_s3_sync`

Sync a git repository to an S3 bucket.

#### Environment variables

| Name                 | Secret | Required | Description                                                   |
|----------------------|--------|----------|---------------------------------------------------------------|
| `BUCKET_CONFIG_NAME` | no     | yes      | name of environment variable with the bucket specification    |
| `BUCKET_CONFIG`      | yes    | yes      | bucket specification as configured in `BUCKET_CONFIG_NAME`    |
| `REPO_URL`           | no     | yes      | git repository to sync                                        |
| `REPO_SUBDIR`        | no     | no       | subdirectory of the git repository to sync, defaults to empty |

### `kernel_configs`

Get newest stable Fedora kernel configuration files to use for upstream kernel builds.

#### Environment variables

| Name                 | Secret | Required | Description                                                   |
|----------------------|--------|----------|---------------------------------------------------------------|
| `BUCKET_CONFIG_NAME` | no     | yes      | name of environment variable with the bucket specification    |

### `pgsql_backup`

Backup a PostgreSQL database to an S3 bucket.
A restore script is provided that reconstructs an SQL dump from the last full or incremental backup.

#### Environment variables

| Name                     | Secret | Required | Description                                                              |
|--------------------------|--------|----------|--------------------------------------------------------------------------|
| `BUCKET_CONFIG_NAME`     | no     | yes      | name of environment variable with the bucket specification               |
| `BUCKET_CONFIG`          | yes    | yes      | bucket specification as configured in `BUCKET_CONFIG_NAME`               |
| `PG_USERNAME`            | no     | yes      | PostgreSQL user name                                                     |
| `PG_PASSWORD`            | yes    | yes      | PostgreSQL password                                                      |
| `PG_HOST`                | no     | yes      | PostgreSQL host                                                          |
| `PG_DB`                  | no     | yes      | PostgreSQL database                                                      |
| `BACKUP_FULL_MAX`        | no     | no       | maximum number of full backups to keep, defaults to 7                    |
| `BACKUP_INCREMENTAL_MAX` | no     | no       | number of incremental backups until a full backup is made, defaults to 7 |

### `example_shell_bot`

Show defined environment variables and test whether the data directory is
writable.

## Development

### Locally building the schedule-bot image

```shell
podman run \
    -v .:/code \
    -w /code \
    -e IMAGE_NAME=schedule-bot \
    --privileged \
    -v ~/.local/share/containers:/var/lib/containers \
    registry.gitlab.com/cki-project/containers/buildah:latest \
    cki_build_image.sh
```

[Data Warehouse]: https://gitlab.com/cki-project/datawarehouse
