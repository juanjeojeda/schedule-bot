#!/bin/bash
set -euo pipefail

# expects the following variables defined in the environment:
# - BUCKET_CONFIG_NAME: env variable with bucket configuration
#
# Restores the last incremental or full backup as an SQL file.
#
# The SQL file can be loaded into a database with something like
#
# (
#     echo 'DO $$ DECLARE'
#     echo '    r RECORD;'
#     echo 'BEGIN'
#     echo '    FOR r IN (SELECT tablename FROM pg_tables WHERE schemaname = current_schema()) LOOP'
#     echo "        EXECUTE 'DROP TABLE ' || quote_ident(r.tablename) || ' CASCADE';"
#     echo '    END LOOP;'
#     echo 'END $$;'
#     cat "/tmp/backup.sql"
# ) | PGPASSWORD=$PG_PASSWORD psql --host $PG_HOST --username $PG_USERNAME --dbname $PG_DB

source utils.sh

parse_bucket_spec
AWS_S3="aws s3 --endpoint ${AWS_ENDPOINT}"
S3_PATH="s3://${AWS_BUCKET}/${AWS_BUCKET_PATH}"

echo_yellow "Retrieving lists of previous backups..."
S3_FULL_LIST="${SCHEDULER_DATA_DIR}/s3-full-list.txt"
S3_INCREMENTAL_LIST="${SCHEDULER_DATA_DIR}/s3-incremental-list.txt"
($AWS_S3 ls "${S3_PATH}full/" || true) | awk '{print $NF}' > "${S3_FULL_LIST}"
($AWS_S3 ls "${S3_PATH}incremental/" || true) | awk '{print $NF}' > "${S3_INCREMENTAL_LIST}"
S3_FULL_COUNT="$(grep -c 'gz$' "${SCHEDULER_DATA_DIR}/s3-full-list.txt" || true)"
S3_INCREMENTAL_COUNT="$(grep -c 'gz$' "${SCHEDULER_DATA_DIR}/s3-incremental-list.txt" || true)"
echo "  ${S3_FULL_COUNT} full and ${S3_INCREMENTAL_COUNT} incremental backups found"

if [ "${S3_INCREMENTAL_COUNT}" -gt 0 ]; then
    echo_yellow "Restoring from incremental backup..."
    LAST_INCREMENTAL="$(tail -n 1 "${S3_INCREMENTAL_LIST}")"
    echo "  Downloading last incremental backup from ${LAST_INCREMENTAL}..."
    $AWS_S3 cp "${S3_PATH}incremental/${LAST_INCREMENTAL}" - | gzip -d > /tmp/last-incremental.sql.diff
    MATCHING_FULL="${LAST_INCREMENTAL%-diff*}.dump.gz"
    echo "  Downloading matching full backup from ${MATCHING_FULL}..."
    $AWS_S3 cp "${S3_PATH}full/${MATCHING_FULL}" - | gzip -d > /tmp/last-full.sql
    echo "  Restoring dump..."
    patch -o /tmp/backup.sql /tmp/last-full.sql /tmp/last-incremental.sql.diff
    echo_green "  successfully restored at /tmp/backup.sql"
else
    echo_yellow "Restoring from full backup..."
    LAST_FULL="$(tail -n 1 "${S3_FULL_LIST}")"
    echo "  Downloading last full backup from ${LAST_FULL}..."
    $AWS_S3 cp "${S3_PATH}full/${LAST_FULL}" - | gzip -d > /tmp/backup.sql
    echo_green "  successfully restored at /tmp/backup.sql"
fi
