#!/bin/bash
set -euo pipefail

# expects the following variables defined in the environment:
# - BUCKET_CONFIG_NAME: env variable with bucket configuration
# - PG_{USERNAME,PASSWORD,HOST,DB}: PostgreSQL
# - BACKUP_FULL_MAX: maximum number of full backups to keep, defaults to 7
# - BACKUP_INCREMENTAL_MAX: number of incremental backups until a full backup is made, defaults to 7

source utils.sh

parse_bucket_spec
AWS_S3="aws s3 --endpoint ${AWS_ENDPOINT}"
S3_PATH="s3://${AWS_BUCKET}/${AWS_BUCKET_PATH}"
BACKUP_FULL_MAX="${BACKUP_FULL_MAX:-7}"
BACKUP_INCREMENTAL_MAX="${BACKUP_INCREMENTAL_MAX:-7}"

PG_DUMP=pg_dump

echo_yellow "Retrieving lists of previous backups..."

S3_FULL_LIST="${SCHEDULER_DATA_DIR}/s3-full-list.txt"
($AWS_S3 ls "${S3_PATH}full/" || true) | awk '{print $NF}' > "${S3_FULL_LIST}"
S3_FULL_COUNT="$(grep -c 'gz$' "${SCHEDULER_DATA_DIR}/s3-full-list.txt" || true)"

S3_INCREMENTAL_LIST="${SCHEDULER_DATA_DIR}/s3-incremental-list.txt"
($AWS_S3 ls "${S3_PATH}incremental/" || true) | awk '{print $NF}' > "${S3_INCREMENTAL_LIST}"
S3_INCREMENTAL_COUNT="$(grep -c 'gz$' "${SCHEDULER_DATA_DIR}/s3-incremental-list.txt" || true)"

S3_INCREMENTAL_LAST_LIST="${SCHEDULER_DATA_DIR}/s3-incremental-last-list.txt"
if [ "${S3_FULL_COUNT}" -gt 0 ]; then
    LAST_FULL="$(tail -n 1 "${S3_FULL_LIST}")"
    ($AWS_S3 ls "${S3_PATH}incremental/${LAST_FULL%.dump.gz}" || true) | awk '{print $NF}' > "${S3_INCREMENTAL_LAST_LIST}"
    S3_INCREMENTAL_LAST_COUNT="$(grep -c 'gz$' "${SCHEDULER_DATA_DIR}/s3-incremental-last-list.txt" || true)"
else
    S3_INCREMENTAL_LAST_COUNT=0
fi

echo "  ${S3_FULL_COUNT} full, ${S3_INCREMENTAL_COUNT} incremental and ${S3_INCREMENTAL_LAST_COUNT} incremental-since-last-full backups found"

if [ "${S3_INCREMENTAL_LAST_COUNT}" -lt "${BACKUP_INCREMENTAL_MAX}" ] && \
   [ "${S3_FULL_COUNT}" -gt 0 ]; then
    echo_yellow "Creating incremental backup..."
    echo "  Downloading last full backup from ${LAST_FULL}..."
    $AWS_S3 cp "${S3_PATH}full/${LAST_FULL}" - | gzip -d > "${SCHEDULER_DATA_DIR}/last-full.sql"
    echo "  Dumping database..."
    PGPASSWORD="${PG_PASSWORD}" ${PG_DUMP} --user "${PG_USERNAME}" --host "${PG_HOST}" "${PG_DB}" > "${SCHEDULER_DATA_DIR}/backup.sql"
    echo "  Determining diff to last full backup..."
    diff -u "${SCHEDULER_DATA_DIR}/last-full.sql" "${SCHEDULER_DATA_DIR}/backup.sql" > "${SCHEDULER_DATA_DIR}/backup.sql.diff" || true
    echo "  Compressing and uploading dump..."
    NAME="${S3_PATH}incremental/${LAST_FULL%.dump.gz}-diff-$(date +'%F-%H-%M').dump.gz"
    gzip -c "${SCHEDULER_DATA_DIR}/backup.sql.diff" | $AWS_S3 cp - "${NAME}"
    echo_green "  successfully created at ${NAME}"
else
    echo_yellow "Creating full backup..."
    NAME="${S3_PATH}full/$(date +'%F-%H-%M').dump.gz"
    echo "  Dumping database..."
    PGPASSWORD="${PG_PASSWORD}" ${PG_DUMP} --user "${PG_USERNAME}" --host "${PG_HOST}" "${PG_DB}" > "${SCHEDULER_DATA_DIR}/backup.sql"
    echo "  Compressing and uploading dump..."
    gzip -c "${SCHEDULER_DATA_DIR}/backup.sql" | $AWS_S3 cp - "${NAME}"
    echo_green "  successfully created at ${NAME}"
fi

echo_yellow "Removing previous backups..."
S3_FULL_LIST="${SCHEDULER_DATA_DIR}/s3-full-list.txt"
S3_INCREMENTAL_LIST="${SCHEDULER_DATA_DIR}/s3-incremental-list.txt"
($AWS_S3 ls "${S3_PATH}full/" || true) | awk '{print $NF}' > "${S3_FULL_LIST}"
($AWS_S3 ls "${S3_PATH}incremental/" || true) | awk '{print $NF}' > "${S3_INCREMENTAL_LIST}"
S3_FULL_COUNT="$(grep -c 'gz$' "${SCHEDULER_DATA_DIR}/s3-full-list.txt" || true)"
S3_INCREMENTAL_COUNT="$(grep -c 'gz$' "${SCHEDULER_DATA_DIR}/s3-incremental-list.txt" || true)"
if [ "${S3_FULL_COUNT}" -gt "${BACKUP_FULL_MAX}" ]; then
    TO_REMOVE="$(head -n "-${BACKUP_FULL_MAX}" "${S3_FULL_LIST}")"
    # no quotes to get word splitting
    for i in ${TO_REMOVE}; do
        echo_red "  removing $i..."
        $AWS_S3 rm "${S3_PATH}full/$i"
    done
else
    echo_green "  no full backups to remove"
fi
if [ "${S3_INCREMENTAL_COUNT}" -gt "${BACKUP_INCREMENTAL_MAX}" ]; then
    TO_REMOVE="$(head -n "-${BACKUP_INCREMENTAL_MAX}" "${S3_INCREMENTAL_LIST}")"
    # no quotes to get word splitting
    for i in ${TO_REMOVE}; do
        echo_red "  removing $i..."
        $AWS_S3 rm "${S3_PATH}incremental/$i"
    done
else
    echo_green "  no incremental backups to remove"
fi
