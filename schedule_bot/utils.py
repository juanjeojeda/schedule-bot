"""Common schedule bot helpers."""
import collections

from cki_lib import misc


def cleanup_gitlab(deletable_objects):
    """Remove merge requests, branches and projects from a GitLab instance."""
    for deletable_object in deletable_objects:
        with misc.only_log_exceptions():
            deletable_object.delete()


# pylint: disable=too-few-public-methods
class Colors:
    """ANSI escape sequences for color output."""

    RED = '\033[1;31m'
    GREEN = '\033[1;32m'
    YELLOW = '\033[1;33m'
    END = '\033[0m'


def cprint(color: Colors, *args, **kwargs):
    """Print colored messages."""
    print(*(f'{color}{str(a)}{Colors.END}' for a in args), **kwargs)


BucketSpec = collections.namedtuple('BucketSpec',
                                    ['endpoint', 'access_key', 'secret_key',
                                     'bucket', 'prefix'])


def parse_bucket_spec(bucket_spec) -> BucketSpec:
    """Parse a deployment-all-style bucket specification."""
    endpoint, access_key, secret_key, bucket, bucket_path = bucket_spec.split(
        '|')
    endpoint = endpoint.rstrip('/') or 'http://s3.amazonaws.com'
    bucket = bucket.rstrip('/')
    bucket_path = bucket_path.rstrip('/') + '/' if bucket_path else ''
    return BucketSpec(endpoint, access_key, secret_key, bucket, bucket_path)
