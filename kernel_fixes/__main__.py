"""Create a kernel-fixes.json file as needed by the pipeline."""
import json
import os
import re
import subprocess

import boto3
import botocore
import git

from schedule_bot import utils


def get_kernel_repo(data_directory, kernel_filename, bucket_spec):
    """Get and update a kernel repo from the git-cache S3 bucket."""
    repo_directory = f'{data_directory}/repo'
    os.makedirs(repo_directory)

    git_cache = utils.parse_bucket_spec(bucket_spec)
    cp_from = f's3://{git_cache.bucket}/{git_cache.prefix}{kernel_filename}'
    aws = ['aws', 's3', 'cp', cp_from, '-', '--endpoint', git_cache.endpoint]
    tar = ['tar', '-xf', '-', '-C', repo_directory]
    env = dict(os.environ)
    env['AWS_ACCESS_KEY_ID'] = git_cache.access_key
    env['AWS_SECRET_ACCESS_KEY'] = git_cache.secret_key

    aws_process = subprocess.Popen(aws, stdout=subprocess.PIPE, env=env)
    subprocess.run(tar, stdin=aws_process.stdout, check=True)

    repo = git.Repo(repo_directory)
    repo.remote('origin').update()

    return repo


def generate_fixes(repo, max_commits):
    """Generate a kernel-fixes.json file."""
    pattern = re.compile(r'^Fixes:\s*(?:commit )?([0-9a-f]{5,})', re.MULTILINE)
    last_checked = None
    fixes = {}
    for commit in repo.iter_commits('master',
                                    max_count=max_commits):
        id_summary = f'{commit!s:.12} {commit.summary}'
        if not last_checked:
            last_checked = id_summary
        for group in re.findall(pattern, commit.message):
            fixes.setdefault(group,
                             {'commitid': []})['commitid'].append(id_summary)
    return {'Lastchecked': last_checked, 'Fixes': fixes}


def put_s3(bucket_spec, filename, data):
    """Upload a file to an S3 bucket."""
    output = utils.parse_bucket_spec(bucket_spec)
    boto_config = botocore.config.Config()
    if not output.access_key:
        boto_config.signature_version = botocore.UNSIGNED
    output_s3 = boto3.resource('s3',
                               endpoint_url=output.endpoint or None,
                               aws_access_key_id=output.access_key,
                               aws_secret_access_key=output.secret_key,
                               config=boto_config)
    output_bucket = output_s3.Bucket(output.bucket)
    output_bucket.put_object(Body=data, Key=f'{output.prefix}{filename}')


def main(config):
    """Parse fixes tags from commit messages."""
    utils.cprint(utils.Colors.YELLOW, 'Getting kernel git repository...')
    repo = get_kernel_repo(os.environ['SCHEDULER_DATA_DIR'],
                           config['kernel']['filename'],
                           os.environ[config['kernel']['bucket_spec']])

    utils.cprint(utils.Colors.YELLOW, 'Generating kernel fixes file...')
    fixes = generate_fixes(
        repo, max_commits=config['kernel'].get('max_commits', 10000))

    utils.cprint(utils.Colors.YELLOW, 'Uploading to S3...')
    put_s3(os.environ[config['output']['bucket_spec']],
           config['output'].get('filename', 'kernel-fixes.json'),
           json.dumps(fixes, indent=4).encode('utf8'))


main(json.loads(os.environ['SCHEDULER_CONFIG']))
