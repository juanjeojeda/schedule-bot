#!/bin/bash
set -euo pipefail

# expects the following variables defined in the environment:
# - BUCKET_CONFIG_NAME
# - BUCKET_CONFIG
# - REPO_URL
# - REPO_SUBDIR

source utils.sh

REPO_TEMPDIR="${SCHEDULER_DATA_DIR}/repo-tempdir/"
REPO_REFS="git-refs.md5"

parse_bucket_spec
AWS_S3="aws s3 --endpoint ${AWS_ENDPOINT}"
S3_PATH="s3://${AWS_BUCKET}/${AWS_BUCKET_PATH}"
S3_CONTENTS="$( ($AWS_S3 ls "${S3_PATH}" || true) | awk '{print $NF}')"

echo_yellow "Checking whether an update is needed.... "
echo "  Calculating checksum of git refs..."
REPO_REFS_MD5="$(git ls-remote --refs "${REPO_URL}" | md5sum)"
echo -n "  Looking for ${REPO_REFS} in S3... "
if grep -E "^${REPO_REFS}$" <<< "${S3_CONTENTS}" > /dev/null; then
    REPO_REFS_MD5_OLD="$($AWS_S3 cp --no-progress "${S3_PATH}${REPO_REFS}" -)"
else
    REPO_REFS_MD5_OLD="nonexistent"
fi

if [ "${REPO_REFS_MD5_OLD}" = "${REPO_REFS_MD5}" ]; then
    echo_green "up to date"
    exit 0
elif [ "${REPO_REFS_MD5_OLD}" = "nonexistent" ]; then
    echo_red "not found"
else
    echo_red "outdated"
fi

echo_yellow "Cloning ${REPO_URL}..."
rm -rf "${REPO_TEMPDIR}"
mkdir -p "${REPO_TEMPDIR}"
git clone "${REPO_URL}" "${REPO_TEMPDIR}"

echo_yellow "Uploading to ${S3_PATH}..."

echo "  Syncing repository..."
$AWS_S3 sync --no-progress --exclude '.git*' "${REPO_TEMPDIR}${REPO_SUBDIR:-}" "${S3_PATH}" --delete --size-only
echo "  Uploading ${REPO_REFS}..."
$AWS_S3 cp --no-progress - "${S3_PATH}${REPO_REFS}" <<< "${REPO_REFS_MD5}"
